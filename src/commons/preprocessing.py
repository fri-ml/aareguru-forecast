import pandas as pd


def process_timestamp(df, col):
    # convert data type
    df[col] = pd.to_datetime(df[col], format="%Y-%m-%d %H:%M:%S")
    # set timestamp as index
    df.set_index(col, inplace=True)
    df.sort_index(inplace=True)
