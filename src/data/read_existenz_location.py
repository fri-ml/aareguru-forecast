# -*- coding: utf-8 -*-
import csv
import json
import logging
from datetime import datetime
from pathlib import Path

import click
import mysql.connector
from dotenv import find_dotenv, load_dotenv


def detail_keys(l):
    s = set()
    for i in l:
        s |= set(i["details"].keys())
    return sorted(list(s))


@click.command()
@click.option("-h", "--db-host", default="existenz-db.konzept.space")
@click.option("-u", "--db-user", default="existenz_api_ro")
@click.option("-p", "--db-password", default=None)
@click.option("-n", "--db-name", default="existenz_api")
@click.option(
    "-t", "--type", "typ", type=click.Choice(["hydro", "smn"]), default="hydro"
)
@click.argument("output_filepath", default="data/raw/", type=click.Path())
def main(db_host, db_user, db_password, db_name, typ, output_filepath):
    logger = logging.getLogger(__name__)

    if db_password is None:
        raise click.BadParameter("Please, set the database password")

    logger.info("Connecting to database at %s", db_host)

    cnx = mysql.connector.connect(
        user=db_user, password=db_password, host=db_host, database=db_name
    )
    cursor = cnx.cursor(buffered=True)

    logger.info("Reading {0} station location".format(typ))
    loc = dict()
    query = "SELECT id, name, details FROM myts_{0}_loc".format(typ)
    cursor.execute(query)
    for r in cursor:
        loc[r[0]] = {"id": r[0], "name": r[1], "details": json.loads(r[2])}

    all_keys = detail_keys(loc.values())
    out_file = Path(output_filepath) / "myts_{0}_loc.csv".format(typ)
    logger.info("writing %s", out_file)

    with open(out_file, "w", newline="") as csvfile:
        csvwriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow(["id", "name", *all_keys])
        for l in loc.values():
            csvwriter.writerow(
                [l["id"], l["name"]] + [l["details"][i] for i in all_keys]
            )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main(auto_envvar_prefix="EXISTENZ")
