# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import mysql.connector
import json
import csv
from datetime import datetime


def detail_keys(l):
    s = set()
    for i in l:
        s |= set(i["details"].keys())
    return sorted(list(s))


@click.command()
@click.option("-h", "--db-host", default="existenz-db.konzept.space")
@click.option("-u", "--db-user", default="existenz_api_ro")
@click.option("-p", "--db-password", default=None)
@click.option("-n", "--db-name", default="existenz_api")
@click.option(
    "-t", "--type", "typ", type=click.Choice(["hydro", "smn"]), default="hydro"
)
@click.argument("station", type=int)
@click.argument("output_filepath", default="data/raw/", type=click.Path())
def main(db_host, db_user, db_password, db_name, typ, station, output_filepath):
    logger = logging.getLogger(__name__)

    if db_password is None:
        raise click.BadParameter("Please, set the database password")

    logger.info("Connecting to database at %s", db_host)

    cnx = mysql.connector.connect(
        user=db_user, password=db_password, host=db_host, database=db_name
    )

    cursor = cnx.cursor(buffered=True)

    logger.info("Reading parameter definition")
    params = dict()
    query = "SELECT id, name, unit, details FROM myts_{0}_par".format(typ)
    cursor.execute(query)
    for r in cursor:
        params[r[0]] = {
            "id": r[0],
            "name": r[1],
            "unit": r[2],
            "details": json.loads(r[3]),
        }

    logger.info("Reading values")
    data = dict()
    query = "SELECT timestamp, loc, par, val FROM myts_{0}_val WHERE loc=%s".format(typ)
    cursor.execute(query, (station,))
    for r in cursor:
        ts = r[0]
        par = params[r[2]]["name"]
        if ts not in data:
            data[ts] = dict()
        data[ts][par] = r[3]

    out_file = Path(output_filepath) / "data_{0}_station_{1}.csv".format(typ, station)
    logger.info("writing %s", out_file)
    all_keys = sorted([i["name"] for i in params.values()])

    with open(out_file, "w", newline="") as csvfile:
        csvwriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow(["ts", *all_keys])
        for l in sorted(data.keys()):
            ts = datetime.utcfromtimestamp(l)
            d = [data[l].get(i, "NaN") for i in all_keys]
            d = [i if i is not None else "NaN" for i in d]
            d = [float(i) for i in d]
            csvwriter.writerow([ts.isoformat()] + d)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main(auto_envvar_prefix="EXISTENZ")
